'use strict';

/**
 * @ngdoc overview
 * @name yoLibreriaApp
 * @description
 * # yoLibreriaApp
 *
 * Main module of the application.
 */
angular
  .module('yoLibreriaApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/libro', {
        templateUrl : 'views/libro.html',
        controller : 'LibroCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });

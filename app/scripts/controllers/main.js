'use strict';

/**
 * @ngdoc function
 * @name yoLibreriaApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the yoLibreriaApp
 */
angular.module('yoLibreriaApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

'use strict';


/**
* yoLibreriaApp Module
*
* Description
*/
angular.module('yoLibreriaApp')
  .controller('LibroCtrl', function ($scope) {
      $scope.libro = {
        "title" : "El hombre en busca de sentido",
        "price" : "99.99",
        "author" : "Victor Frankl",
        "type" : ["Drama", "Historia", "Cuentos"],
        "description" : "Esto es una pequeña narración"
      };
      $scope.comments = [];
      $scope.comments = {};
      $scope.show = false;
      $scope.toggle = function (){
        $scope.show = !$scope.show;
      };
      this.anonymousChanged = function (){
        if ($scope.comment.anonymous){
          $scope.comment.email="";
        }
      };
  });

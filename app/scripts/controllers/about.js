'use strict';

/**
 * @ngdoc function
 * @name yoLibreriaApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the yoLibreriaApp
 */
angular.module('yoLibreriaApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
